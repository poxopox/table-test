import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TypeaheadTableComponent} from "./typeahead-table/typeahead-table.component";
import {MultiSelectFilterTableComponent} from "./multi-select-filter-table/multi-select-filter-table.component";

const routes: Routes = [
  {path: 'typeahead', component: TypeaheadTableComponent},
  {path: 'multi-select', component: MultiSelectFilterTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
