import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {MockData} from "./mock-data.interface";
import {MOCK_DATA} from "./MOCK_DATA (1)";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() {
  }


  getData$(): Observable<Array<MockData>> {
    return of(MOCK_DATA);
  }

}
