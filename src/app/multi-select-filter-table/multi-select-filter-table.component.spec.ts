import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiSelectFilterTableComponent } from './multi-select-filter-table.component';

describe('MultiSelectFilterTableComponent', () => {
  let component: MultiSelectFilterTableComponent;
  let fixture: ComponentFixture<MultiSelectFilterTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiSelectFilterTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectFilterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
