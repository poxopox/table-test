import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { DataService } from "../data/data.service.ts.service";
import { FormBuilder, FormControl } from "@angular/forms";
import {
  concatAll,
  distinct,
  filter,
  mergeMap,
  pluck,
  startWith,
  tap,
  toArray
} from "rxjs/operators";
import { combineLatest, from } from "rxjs";
import { MOCK_DATA } from "../data/MOCK_DATA (1)";

@Component({
  selector: "app-multi-select-filter-table",
  templateUrl: "./multi-select-filter-table.component.html",
  styleUrls: ["./multi-select-filter-table.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiSelectFilterTableComponent implements OnInit {
  displayedColumns = [
    "id",
    "first_name",
    "last_name",
    "email",
    "gender",
    "ip_address"
  ];
  displayedColumnsCopy = [...this.displayedColumns];

  filterForm = this.fb.array(
    [...this.displayedColumns].map((x, i) => {
      return new FormControl(MOCK_DATA.map(mock => mock[x]));
    })
  );
  dataSource$ = combineLatest(
    this.dataService.getData$(),
    this.filterForm.valueChanges.pipe(
      startWith(
        [...this.displayedColumns].map((x, i) => {
          return MOCK_DATA.map(mock => mock[this.displayedColumns[i]]);
        })
      )
    )
  ).pipe(
    mergeMap(([mockData, form]) => {
      console.time("Multiselect Filter");
      return from(mockData).pipe(
        filter(point => {
          let result = false;
          for (let i = 0; i < this.displayedColumns.length; i++) {
            result = form[i].includes(point[this.displayedColumns[i]]);
            if (!result) return false;
          }
          return result;
        }),
        toArray(),
        tap(() => console.timeEnd("Multiselect Filter"))
      );
    })
  );

  constructor(public dataService: DataService, public fb: FormBuilder) {}

  ngOnInit() {}

  getSelections$(columnName: string) {
    return this.dataService.getData$().pipe(
      concatAll(),
      distinct(x => x[columnName]),
      pluck(columnName),
      toArray()
    );
  }
}
