import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeaheadTableComponent } from './typeahead-table.component';

describe('TypeaheadTableComponent', () => {
  let component: TypeaheadTableComponent;
  let fixture: ComponentFixture<TypeaheadTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeaheadTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeaheadTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
