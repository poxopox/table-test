import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {DataService} from "../data/data.service.ts.service";
import {FormBuilder, FormControl} from "@angular/forms";
import {combineLatest, from} from "rxjs";
import {concatAll, distinct, filter, mergeMap, pluck, startWith, tap, toArray} from "rxjs/operators";
import longestCommonSubstring from "../longestCommonSubstring";

@Component({
  selector: "app-typeahead-table",
  templateUrl: "./typeahead-table.component.html",
  styleUrls: ["./typeahead-table.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypeaheadTableComponent implements OnInit {
  displayedColumns = [
    "id",
    "first_name",
    "last_name",
    "email",
    "gender",
    "ip_address"
  ];
  displayedColumnsCopy = [...this.displayedColumns];

  filterForm = this.fb.array(
    [...this.displayedColumns].map((x, i) => {
      return new FormControl();
    })
  );
  dataSource$ = combineLatest(
    this.dataService.getData$(),
    this.filterForm.valueChanges.pipe(
      startWith(
        [...this.displayedColumns].map((x, i) => {
          return "";
        })
      )
    )
  ).pipe(
    mergeMap(([mockData, form]) => {
      console.time("Typeahead Filter Start");
      return from(mockData).pipe(
        filter(point => {
          let result = false;
          for (let i = 0; i < this.displayedColumns.length; i++) {
            result =
              !form[i] ||
              longestCommonSubstring(point[this.displayedColumns[i]] + "", form[i].toLowerCase()).length > 0;
            if (!result) return false;
          }
          return result;
        }),
        toArray(),
        tap(() => console.timeEnd("Typeahead Filter Start"))
      );
    })
  );

  constructor(public dataService: DataService, public fb: FormBuilder) {
  }

  ngOnInit() {
  }

  getSelections$(columnName: string) {
    return this.dataService.getData$().pipe(
      concatAll(),
      distinct(x => x[columnName]),
      pluck(columnName),
      toArray()
    );
  }
}
